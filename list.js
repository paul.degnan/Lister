function addToList(){

	var listHolder = document.getElementById( "list_holder" );

	//if this is the first list item, we need to add the parent UL element
	var ulElement;
	if( listHolder.childNodes.length === 0 ){

		ulElement = document.createElement( "ul" );
		ulElement.id = "list_entries";
		listHolder.appendChild( ulElement );
	
	} else {

		ulElement = document.getElementById( "list_entries" );

	}

	var input = document.getElementById( "list_input" );

	var liElement = document.createElement( "li" );
	liElement.innerHTML = input.value;

	ulElement.appendChild( liElement );
}

function removeFromList( evt ){

	if ( evt.target.nodeName == "LI" ){

		var ulElement = document.getElementById( "list_entries" );
		ulElement.removeChild( evt.target );		

	}

	//remove parent UL if it's empty
	if ( ulElement.childNodes.length === 0 ){

		ulElement.parentNode.removeChild( ulElement );

	}

}

function submitForm( evt ){

	var ulElement = document.getElementById( "list_entries" );
	if ( ulElement  ){

		var hiddenInput = document.createElement( "input" );
		hiddenInput.type = "hidden";
		hiddenInput.name = "list_items";

		var listItemsArray = [];
		for ( var i = 0; i < ulElement.childNodes.length; i++ ){

			listItemsArray.push( ulElement.childNodes[ i ].innerHTML )

		}
		hiddenInput.value = JSON.stringify( listItemsArray );
		evt.target.appendChild( hiddenInput );

	}


}

function createLister(){

	var parentDiv = document.getElementById( "list_maker" );
	if( typeof( parentDiv ) !== "undefined" ){

		var inputHolderDiv = document.createElement( "div" );
		inputHolderDiv.id = "input_holder";
		var listInput = document.createElement( "input" );
		listInput.type = "text";
		listInput.id = "list_input";
		inputHolderDiv.appendChild( listInput );		

		var addButtonDiv = document.createElement( "div" );
		addButtonDiv.id = "add_button";
		addButtonDiv.innerHTML = "[add]";

		var listHolderDiv = document.createElement( "div" );
		listHolderDiv.id = "list_holder";

		var listForm  = document.createElement( "form" );
		listForm.id = "list_form";
		var submitInput = document.createElement( "input" );
		submitInput.type = "submit";
		submitInput.value = "submit";
		listForm.appendChild( submitInput );

		parentDiv.appendChild( inputHolderDiv );
		parentDiv.appendChild( addButtonDiv );
		parentDiv.appendChild( listHolderDiv );
		parentDiv.appendChild( listForm );

	}

	var add = document.getElementById( "add_button" );
	add.addEventListener( "click", addToList );

	var list = document.getElementById( "list_holder" );
	list.addEventListener( "click", removeFromList )

	var form = document.getElementById( "list_form" );
	form.addEventListener( "submit", submitForm );

}

window.addEventListener( "load", function(){ 
	
	createLister();

} );

