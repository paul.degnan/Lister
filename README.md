# A javascript utility that creates an input and "add" button for adding arbitrary user-created elements to a form. Submits values as an array.

To use:

- add the list.js file to your HTML as in the example below (or the index.html file in the project)
- Add a `<div>` tag to your HTML with an ID of list_maker as in the example below (or the index.html file in the project). 
- That's it! 
- P.S. also it's highly recommended that you use the styles in list.css. The utility will work without them, but not look as good.

You can see a working example at http://sueandpaul.com/lister/

# Wishlist / todos:

- Not sure that it's correct for this to add its own submit button. Many users may want to insert this into an existing form
- Also should create a object to wrap the functionality and allow it to be used in more than one element on the same page.

```html
<pre>
<html>
<head>
	<script type="text/javascript" src="list.js"></script>
	<link href="list.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="list_maker"></div>
</body>
</html>
</pre>
```